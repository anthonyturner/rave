//
//  FirstViewController.swift
//  Rave
//
//  Created by Anthony Turner on 3/21/16.
//  Copyright © 2016 Anthony Turner. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    //var audioPlayer = STKAudioPlayer()
    var songs = [NSString]()
    var startMusicApp = Bool!()
    var isStopped = Bool!()
    var songIsFinished = Bool!()
    var trackNumber = NSInteger!()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        songs = ["Renegade master", "Sand Storm", "This is Rave"]
        startMusicApp = false
        isStopped = false
        songIsFinished = false
        trackNumber = 0
        isBuffering.text = ""
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var isBuffering: UILabel!
    @IBOutlet weak var currentSong: UILabel!
    @IBOutlet weak var songTableView: UITableView!
    
    @IBAction func playButton(sender: AnyObject) {
        print("play")

        
    }
    
    @IBAction func stopButton(sender: AnyObject) {
        print("stop")
        
        
    }
    
    func play(songPosition: NSInteger){
        
        currentSong.text = songs[songPosition] as String

        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell")
        cell?.textLabel?.text = String(indexPath.row) + " - " + (songs[indexPath.row] as String)
        
        currentSong.text = songs[indexPath.row] as String
        // Configure the cell...
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        trackNumber = indexPath.row - 1
        self.play(indexPath.row)
        

    }
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
   }

